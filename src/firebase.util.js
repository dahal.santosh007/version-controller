import firebase from 'firebase/app';
import 'firebase/auth';

const REACT_APP_FIREBASECONFIG = {
    apiKey: "AIzaSyAidx2nS4GlUBUqUVcIfIpac2htFLMXtpA",
    authDomain: "githubreporter-e29fd.firebaseapp.com",
    databaseURL: "https://githubreporter-e29fd.firebaseio.com",
    projectId: "githubreporter-e29fd",
    storageBucket: "githubreporter-e29fd.appspot.com",
    messagingSenderId: "925056073039",
    appId: "1:925056073039:web:d9420e737e4b4aef84193a",
    measurementId: "G-0WLKYEM8B1"
  };

firebase.initializeApp(REACT_APP_FIREBASECONFIG);

export const auth = firebase.auth();

const provider = new firebase.auth.GithubAuthProvider();

provider.setCustomParameters({ prompt: 'select_account' });
export const signInWithGitHub = () => auth.signInWithPopup(provider);

export default firebase;
