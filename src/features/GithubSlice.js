import {createSlice} from '@reduxjs/toolkit';
import axios from 'axios';
import cloneDeep from 'lodash/cloneDeep';

export const githubSlice = createSlice({
    name:'github',
    initialState:{
        userName:[],
        email:[],
        repoList:[],
        repoData:[],
        fullName:[],
        responseData:[],
    },
    reducers:{
        getGithubAccNameReducer:(state,action)=>{
            console.log(action.payload);
            state.userName= action.payload.items;
            state.email=action.payload.email;
        },
        getGithubRepoReducer:(state,action)=>{
            state.repoList=action.payload
        },
        addRepoDataReducer:(state,action)=>{
            let pushableData=[];
            action.payload.map(data=>(
                data.type === "file" ?
                pushableData.push({name:data.name,url:data.download_url,type:data.type})
                :pushableData.push({name:data.name,type:data.type,pathName:`/contents/${data.name}`})
                )
            )
            state.repoData=pushableData
        },
        addFullNameReducer:(state,action)=>{
            state.fullName=action.payload
        },
        addToResponseReducer:(state,action)=>{
            state.responseData = action.payload
        },
        getDirDataReducer:(state,action)=>{
            console.log(action.payload.data[0])
            let currentPath = action.payload.data[0].path.split("/");
            currentPath.pop();
            let pushableData=[]
            let repoCopy = cloneDeep(action.payload.repoData);
            let count = 0;

            const recur = (repocopy,currentpath)=>{
                if(currentpath.length ===0){
                    return null
                }

                function callFunc(repocopypassed,currentpath){
                    if(currentpath.length===1){
                        action.payload.data.map((actionData,index)=>(
                            actionData.type==="file" ?
                            pushableData.push({name:actionData.name,url:actionData.download_url,type:actionData.type})
                            :pushableData.push({name:actionData.name,type:actionData.type,pathName:`/contents/${actionData.path}`})
                        ));
                        repocopypassed.meta=pushableData;
                        currentpath.unshift();
                    }
                }
                
                repocopy.map((repocopypassed,index)=>(
                    (repocopypassed.name===currentpath[count])?(
                        currentpath.length===1?                         
                            callFunc(repocopypassed,currentPath):
                        (
                            console.log(repocopypassed),
                            currentpath.shift(),
                            recur(repocopypassed.meta,currentpath)
                        )
                    ):""
                ))

            }

            recur(repoCopy,currentPath);
        
            state.repoData = repoCopy
        }
    }
})

export default githubSlice.reducer;

const {getGithubAccNameReducer,getGithubRepoReducer,addRepoDataReducer,addFullNameReducer,addToResponseReducer,getDirDataReducer} = githubSlice.actions;

export const getGithubName = (email) => async dispatch=>{
    try {
        console.log('entered getGitHub');
        console.log(email);
        const res = await axios(`https://api.github.com/search/users?q=${email}`);
        const [items] = res.data.items;

        dispatch(getGithubAccNameReducer({items,email:email}))

    } catch (error) {
        console.log(error);
    }
}

export const getGithubRepo = (url) => async dispatch =>{
    try {
        console.log('entered getGithubRepo');

        const res = await axios({
            baseURL:`${url}/repos`,
            auth:{username:process.env.REACT_APP_ACCESS_TOKEN}
        });

        const data = res.data;

        dispatch(getGithubRepoReducer(data));

    } catch (error) {
        console.log(error);
    }
}

export const getRepoData = (repoName) => async dispatch =>{
    try {
        console.log('entered getRepoData');
        console.log(`https://api.github.com/repos/${repoName}/contents/`);

        const res = await axios({
            baseURL:`https://api.github.com/repos/${repoName}/contents/`,
            auth:{username:process.env.REACT_APP_ACCESS_TOKEN}
        });
        
        const [...data] = res.data;
        dispatch(addRepoDataReducer(data))
        dispatch(addFullNameReducer(repoName))
    } catch (error) {
        console.log(error);
    }
}

export const getFileData= ({url})=>async dispatch=>{
    try {
        console.log('entered getFileData');

        const res = await axios(url);
        
        dispatch(addToResponseReducer(res.request.responseText))

    } catch (error) {
        console.log(error)
    }
}

export const getDirData= (fullName,url,repoData)=>async dispatch=>{
    try {
        console.log('entered getDirData');
        console.log(`https://api.github.com/repos/${fullName}${url}`);

        const res = await axios({
            baseURL:`https://api.github.com/repos/${fullName}${url}`,
            auth:{username:process.env.REACT_APP_ACCESS_TOKEN}
        });

        dispatch(getDirDataReducer({data:res.data,repoData:repoData}))

    } catch (error) {
        console.log(error)
    }
}