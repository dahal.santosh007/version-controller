// use octokit
// oauth rather than personal access token
// UI/UX improvements
// For private github repo as well
// Easy push and pull to github repo features like github for desktop
// Show merge and branch information in a much more intuitive way. 

import React from 'react';
import {Provider} from 'react-redux';
import store from './app/store';
import './App.css';
import Login from './component/Login';
import { BrowserRouter as Router, Switch , Route} from 'react-router-dom';
import RepoLookup from './component/RepoLookup';

function App() {
  
  return (
    <Provider store = {store}>
      <Router>
        <div className="App">
          <Switch>
            <Route path = "/RepoLookup/:owner/:selectedRepo" component={RepoLookup}></Route>
            <Route path = "/" exact component={Login}></Route>
          </Switch>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
