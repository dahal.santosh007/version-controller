import {configureStore} from '@reduxjs/toolkit';
import {combineReducers} from 'redux';
import githubReducer from '../features/GithubSlice';

const reducer = combineReducers({
    posts: githubReducer
})

const store = configureStore({
    reducer
})

export default store;