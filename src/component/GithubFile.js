import React from 'react'
import {useSelector} from 'react-redux';
import '../css/GithubFile.css'

const GithubFile = () => {
    const responseText = useSelector(state => state.posts.responseData)
        let str = JSON.stringify(responseText)
        
        let withTag = str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');

        let strr = JSON.parse(withTag).replace(/(?:\t|\r|\n|\v|\f)/g, '<br>');
    
        let finalString = strr.replace(/ /g, '\u00a0');

        return <div className = "test__class" dangerouslySetInnerHTML={{__html: finalString}}/>
}

export default GithubFile
