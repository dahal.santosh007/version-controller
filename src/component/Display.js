import React,{useEffect} from 'react'
import {useSelector,useDispatch} from 'react-redux';
import { getGithubRepo } from '../features/GithubSlice';
import Image from 'react-bootstrap/Image';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import ReposList from './ReposList';
import { MDBIcon} from "mdbreact";
import '@fortawesome/fontawesome-free/css/all.min.css';
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import '../css/Display.css'
import Navigation from './Navigation';

const Display = () => {
    const username = useSelector(state => state.posts.userName);
    const email = useSelector(state => state.posts.email);
    const repoList = useSelector(state=>state.posts.repoList);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getGithubRepo(username.url,username))
        // return () => {
        //     cleanup
        // }
    }, [username,dispatch])

    return (
        <div className = "display">
          <Navigation/>
          <div className="display__list">
            <div className = "display__left">
              <Image className = "display__avatar" src={username.avatar_url} roundedCircle/>
              <br/>
              <br/>
              <div className = "display__username"><MDBIcon far icon="user" className="display__icon"/>{username.login}</div>
              <hr/>
              <div className = "display__email"><MDBIcon far icon="envelope" className="display__icon"/>{email}</div>
            </div>

            <div className="display__right">
              <h5 className = "display__repoText">Repositories</h5>
              <hr/>
              <br/>
              <Container>
                <Row>
                    {
                      repoList.map((repo,id)=>(
                          <ReposList repo= {repo} key = {id}/>
                      ))
                    }
                </Row>
              </Container>
            </div>
          </div>      
        </div>
    )
}

export default Display
