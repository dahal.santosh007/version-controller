import React from 'react';
import {Link} from 'react-router-dom';
import Col from 'react-bootstrap/Col';
import '../css/Repolist.css';

const ReposList = ({repo}) => {
    return (
        <Col sm = {12} md={5} className = "repolist">
            <Link to = {`/RepoLookup/${repo.full_name}`}>{repo.name}</Link>
        </Col>
    )
}

export default ReposList
