import React from 'react'
import { getFileData, getDirData } from '../features/GithubSlice';
import {useDispatch,useSelector} from 'react-redux';
import { MDBIcon} from "mdbreact";

const RepoData = ({items,depth}) => {
    const dispatch = useDispatch();
    const fullName = useSelector(state => state.posts.fullName);
    const repoData = useSelector(state => state.posts.repoData);
    const username = useSelector(state => state.posts.userName);

    if (!items || !items.length){
        return null
    }

    return items.map((item,index)=>(
        <React.Fragment key={index}>
            {item.type === "dir"
                ?
                    (<div>                      
                        <p  className = "dir__class" 
                            style={{ paddingLeft: depth * 15 }} 
                            key={index} 
                            onClick={()=>dispatch(getDirData(fullName,item.pathName,repoData,username.url))}><MDBIcon far icon="folder" />  {item.name}</p>
                    </div>)
                :
                    (<div>
                        <p  className = "file__class"  
                            style={{ paddingLeft: depth * 15 }} 
                            key = {index} 
                            onClick = {()=>dispatch(getFileData({url:item.url,username:username.url}))}><MDBIcon far icon="file" /> {item.name}</p>
                    </div>)
            }
            <RepoData items={item.meta} depth={depth + 1}/>
        </React.Fragment>
        )
    )
}

export default RepoData
