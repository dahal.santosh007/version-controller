import React from 'react'
import {signInWithGitHub} from '../firebase.util';
import Button from 'react-bootstrap/Button';
import Image from 'react-bootstrap/Image';
import '../css/LandingPage.css';

const LandingPage = () => {
    return (
        <div className = "landingpage">
            <div className = "landingpage__loginButton">
                <Image src={require('/Users/user/Documents/Project/reporter/src/images/24079fc9-b714-4fff-be91-1aa7ad644be0_200x200.png')} />
                <Button type = "button" variant="light" size = "lg" onClick ={signInWithGitHub}> SIGN IN WITH GITHUB</Button>
            </div>
            <div className = "landingpage__image">
                <div className="landingpage__circle">
                    <Image className = "landingpage__imgOne" src={require('/Users/user/Documents/Project/reporter/src/images/img1.jpg')}/>
                    <Image className = "landingpage__imgTwo" src={require('/Users/user/Documents/Project/reporter/src/images/img2.png')}/>
                    <Image className = "landingpage__imgThree" src={require('/Users/user/Documents/Project/reporter/src/images/img3.png')} />
                </div>
            </div>
        </div>
    )
}

export default LandingPage
