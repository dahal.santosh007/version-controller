import React, { useEffect } from 'react';
import GithubFile from './GithubFile';
import {useDispatch,useSelector} from 'react-redux';
import {getRepoData} from '../features/GithubSlice';
import '../css/RepoLookup.css';
import RepoData from './RepoData';
import Navigation from './Navigation';

const RepoLookup = ({match}) => {
    const dispatch = useDispatch();
    const repoData = useSelector(state => state.posts.repoData);
    const response = useSelector(state => state.posts.responseData);
    const fullName = `${match.params.owner}/${match.params.selectedRepo}`;

    useEffect(()=>{
        dispatch(getRepoData(`${fullName}`));
    },[dispatch,fullName,match.params.owner])

    return (
        <div className = "repoLookUp__container">
            <Navigation/>
            <div className="repoLookUp__repoListBox">
                <div className ="repoLookUp__left"><RepoData items={repoData} depth={0}/></div>
                <div className ="repoLookUp__right">{response.length?<GithubFile/>:" "}</div>
            </div>
        </div>
    )
}

export default RepoLookup