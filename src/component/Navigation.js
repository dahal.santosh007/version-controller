import React from 'react'
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Image from 'react-bootstrap/Image';
import NavDropdown from 'react-bootstrap/NavDropdown';
import {auth} from '../firebase.util';
import {Link} from 'react-router-dom';
import '../css/Navigation.css';

const Navigation = () => {
    return (
        <div className = "navigation">
            <Navbar bg="primary" variant="light">
                <Image  className = "navigation__logo" 
                        src={require('/Users/user/Documents/Project/reporter/src/images/24079fc9-b714-4fff-be91-1aa7ad644be0_200x200.png')} />
                <Nav className="ml-auto">
                    <Nav.Link><Link to = {'/'} className = "navigation__navLink">Home</Link></Nav.Link>
                    <Nav.Link href="#features" className = "navigation__navItem">Features</Nav.Link>
                    <NavDropdown title="Menu" className = "navigation__navItem" id="basic-nav-dropdown">
                        <NavDropdown.Item href="#action/3.1">UnDecided</NavDropdown.Item>
                        <NavDropdown.Divider />
                        <NavDropdown.Item onClick={()=>auth.signOut()}>
                        Log Out
                        </NavDropdown.Item>
                    </NavDropdown>
                </Nav>
            </Navbar> 
        </div>
    )
}

export default Navigation
