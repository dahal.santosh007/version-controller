import React, { useState,useEffect } from 'react';
import {auth} from '../firebase.util';
import { getGithubName } from '../features/GithubSlice';
import { useDispatch } from 'react-redux';
import Display from './Display';
import LandingPage from './LandingPage';

const Login = () => {
    const [currentUser, setCurrentUser] = useState(null);
    const dispatch = useDispatch();;

    useEffect(() => {
      auth.onAuthStateChanged(user=>{
        setCurrentUser(user)
        dispatch(getGithubName(user.email))
      })

      auth.getRedirectResult().then(function(result) {
        if (result.credential) {
          // This gives you a GitHub Access Token. You can use it to access the GitHub API.
          var token = result.credential.accessToken;
          console.log(token)
          // ...
        }
        // The signed-in user info.
        var user = result.user;
        console.log(user)
      }).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
        // ...
      });
      // return () => {
      //   cleanup
      // }
    }, [dispatch])

    return (
      <div className="login">
        {
          currentUser?
            <Display/>
          :      
            <LandingPage/>
        }
      </div>
    )
}

export default Login
